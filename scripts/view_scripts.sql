-- получение количество людей определённой подписки
create view tariff_cnt as
select tariff_plan, count(*)
from site_hosting_database."user"
group by tariff_plan;

-- получение юзеров без персональных данных
create view mock_users as
select '******' as user_mail,
       '******' as password_hash,
       site_id,
       tariff_plan
from site_hosting_database."user";

-- сводная таблица людей и сайтов

create view users_and_their_sites as
select "user".user_mail, "user".site_id, source_code
from site_hosting_database."user"
         join site_hosting_database.site s on "user".user_mail = s.user_mail;

-- получение количества комментариев к сайту

create view site_comments as
select site.site_id, count(*)
from site_hosting_database.site site
         join site_hosting_database.comment c on site.site_id = c.site_id
group by site.site_id
order by site_id;