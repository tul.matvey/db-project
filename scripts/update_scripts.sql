--Юзер поменял пароль
update site_hosting_database.user
set password_hash = '0x8yc823js9mx8c'
where user_mail = 'Novikov_Sidor@example.com';

--Создатели сайтов 3, 4, 5 выиграли подписку
update site_hosting_database.user
set tariff_plan = 'pro'
where site_id between 3 and 5;

--Сайт 8 переписали
update site_hosting_database.site
set source_code = '#site8_code_new#'
where site_id = 8;

--Комментарий к сайту 10 изменили, который сделал Petrov_Vasilij@example.com
update site_hosting_database.comment
set text = '#comment9_text_new#'
where site_id = 10
  and user_mail = 'Petrov_Vasilij@example.com';

--Обнавили сайты pro юзеров
update site_hosting_database.site
set source_code = '#pro_site_code#'
from (select tariff_plan, user_mail from site_hosting_database.user) as tariff
where tariff.tariff_plan = 'pro'
  and tariff.user_mail = site.user_mail
