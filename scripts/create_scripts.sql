--Создание таблицы

--Создание схемы базы для сервиса по хостингу сайтов

create schema site_hosting_database;

--Информация о тарифном плане

create table site_hosting_database.tariff_plan
(

    name             varchar(255) primary key,
    text_description varchar(255) not null

);

--Информация о юзере

create table site_hosting_database.user
(

    user_mail     varchar(255) primary key,
    password_hash varchar(255) not null,
    site_id       bigint,
    tariff_plan   varchar(255) not null,
    foreign key (tariff_plan) references site_hosting_database.tariff_plan (name)

);

--Информация о сайтах

create table site_hosting_database.stats
(

    stats_id bigserial primary key,
    info     text not null

);

--Информация о сайте

create table site_hosting_database.site
(

    site_id     bigserial primary key,
    source_code text,
    user_mail   varchar(255) not null,
    stats_id    bigint,
    foreign key (user_mail) references site_hosting_database.user (user_mail),
    foreign key (stats_id) references site_hosting_database.stats (stats_id)

);


--Информация о комментариях

create table site_hosting_database.comment
(

    comment_id bigserial primary key,
    text       text,
    user_mail  varchar(255) not null,
    site_id    bigint,
    foreign key (user_mail) references site_hosting_database.user (user_mail),
    foreign key (site_id) references site_hosting_database.site (site_id)

);
