--Количество людей с подписками разного вида
select tariff_plan, count(*)
from site_hosting_database.user
group by tariff_plan;

--Поиск комментариев про html
select text
from site_hosting_database.comment
where text like '%html%';

--Пользователи, которые оставляли не мение 2 комментариев и обладающие подпиской
select site_hosting_database.user.user_mail
from site_hosting_database.user
         join site_hosting_database.comment on "user".user_mail = comment.user_mail
where tariff_plan = 'pro'
group by site_hosting_database.user.user_mail
having count(*) >= 2;

--Вывод пользователей с "плохим" поролем и подпиской pro (пороли считаем плохими если оканчиваются на 8)
select user_mail
from site_hosting_database.user
where substring(password_hash, length(password_hash), 1) = '8'
  and tariff_plan = 'pro';


--Ранжированный список сайтов по количеству комментариев
with comment_cnt as (select site_id, count(*) as cnt
                     from site_hosting_database.comment
                     group by site_id),
     ranked as (select site.site_id, cnt
                from site_hosting_database.site
                         inner join comment_cnt on comment_cnt.site_id = site.site_id)
select site_id, rank() over (order by cnt)
from ranked;

--Получить количество пользователей с паролями максимальной длины с определённой подпиской
with pass_rank as (select user_mail,
                          tariff_plan,
                          rank() over (partition by tariff_plan order by length(password_hash) desc) as cnt
                   from site_hosting_database.user),
     st_place as (select user_mail, tariff_plan
                  from pass_rank
                  where cnt = 1)
select tariff_plan, count(*) as cnt
from st_place
group by tariff_plan;


--Средняя длина кода людей с базовой подпиской
select avg(length(source_code))
from site_hosting_database.site
         inner join site_hosting_database."user" on site.user_mail = "user".user_mail
where tariff_plan = 'base';

--Количество комминтариев для человека
with site_cnt as (select site_id, count(*) as cnt
                  from site_hosting_database.comment
                  group by site_id),
     cnt_person as (select user_mail, sum(cnt) as cnt
                    from site_hosting_database."user"
                             join site_cnt s on "user".site_id = s.site_id
                    group by user_mail)
select user_mail, cnt
from cnt_person;

--Получим людей с базовой подпиской по увеличению длины статистики сайта
select user_mail
from site_hosting_database.site
         inner join site_hosting_database.stats s on site.stats_id = s.stats_id
where (select tariff_plan
       from (select u.user_mail, tariff_plan
             from site_hosting_database.site
                      inner join site_hosting_database."user" u
                                 on site_hosting_database.site.user_mail = u.user_mail) as sub
       where site.user_mail = sub.user_mail) = 'base'
order by length(info);

--Описание тарифа человека с паролем минимальной длины
with min_len as (select tariff_plan,
                        rank() over (partition by tariff_plan order by length(password_hash)) as cnt
                 from site_hosting_database.user
                 limit 1)
select text_description
from min_len inner join site_hosting_database.tariff_plan tp on tp.name = min_len.tariff_plan

