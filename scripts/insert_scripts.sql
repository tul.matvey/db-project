insert into site_hosting_database.tariff_plan (name, text_description)
values ('base', 'free tariff plan with bounded abilities'),
       ('pro', 'premium tariff plan for experienced users');

insert into site_hosting_database.stats (stats_id, info)
values ('1', '#stats1_info#'),
       ('2', '#stats2_info#'),
       ('3', '#stats3_info#'),
       ('4', '#stats4_info#'),
       ('5', '#stats5_info#'),
       ('6', '#stats6_info#'),
       ('7', '#stats7_info#'),
       ('8', '#stats8_info#'),
       ('9', '#stats9_info#'),
       ('10', '#stats10_info#');

insert into site_hosting_database.user (user_mail, password_hash, site_id, tariff_plan)
values ('Egorov_Egor@example.com', '0x190a2517d1e1d5', 8, 'pro'),
       ('Sokolov_Nikolaj@example.com', '0x1c6f80b5db82ad', 9, 'pro'),
       ('Ivanov_Fedor@example.com', '0x1f25515e878955', 10, 'pro'),
       ('Petrov_Vasilij@example.com', '0x1849fc222c9d28', 4, 'pro'),
       ('Novikov_Sidor@example.com', '0x24a52dc7d5ceb', 5, 'pro'),
       ('Vasilyev_Petr@example.com', '0x22db18d46ffa38', 1, 'base'),
       ('Kuznetsov_Ivan@example.com', '0x1796e58c682218', 2, 'pro'),
       ('Smirnov_Kuzyma@example.com', '0x169eb35588ea23', 6, 'pro'),
       ('Fedorov_Stepan@example.com', '0x20e6e3189d2d3a', 3, 'base'),
       ('Sidorov_Semen@example.com', '0x1a81d967099ea', 7, 'pro');

insert into site_hosting_database.site (site_id, source_code, user_mail, stats_id)
values (8, '#site8_code#', 'Egorov_Egor@example.com', 2),
       (9, '#site9_code#', 'Sokolov_Nikolaj@example.com', 7),
       (10, '#site10_code#', 'Ivanov_Fedor@example.com', 10),
       (4, '#site4_code#', 'Petrov_Vasilij@example.com', 3),
       (5, '#site5_code#', 'Novikov_Sidor@example.com', 1),
       (1, '#site1_code#', 'Vasilyev_Petr@example.com', 9),
       (2, '#site2_code#', 'Kuznetsov_Ivan@example.com', 5),
       (6, '#site6_code#', 'Smirnov_Kuzyma@example.com', 4),
       (3, '#site3_code#', 'Fedorov_Stepan@example.com', 6),
       (7, '#site7_code#', 'Sidorov_Semen@example.com', 8);

insert into site_hosting_database.comment (comment_id, text, user_mail, site_id)
values (1, '#comment1_text#', 'Smirnov_Kuzyma@example.com', 10),
       (2, '#comment2_text#', 'Kuznetsov_Ivan@example.com', 2),
       (3, '#comment3_text#', 'Ivanov_Fedor@example.com', 5),
       (4, '#comment4_text#', 'Vasilyev_Petr@example.com', 10),
       (5, '#comment5_text#', 'Smirnov_Kuzyma@example.com', 6),
       (6, '#comment6_text#', 'Novikov_Sidor@example.com', 4),
       (7, '#comment7_text#', 'Petrov_Vasilij@example.com', 3),
       (8, '#comment8_text#', 'Smirnov_Kuzyma@example.com', 4),
       (9, '#comment9_text#', 'Petrov_Vasilij@example.com', 10),
       (10, '#comment10_text#', 'Kuznetsov_Ivan@example.com', 6),
       (11, '#comment11_text#', 'Smirnov_Kuzyma@example.com', 4),
       (12, '#comment12_text#', 'Ivanov_Fedor@example.com', 10),
       (13, '#comment13_text#', 'Fedorov_Stepan@example.com', 4),
       (14, '#comment14_text#', 'Egorov_Egor@example.com', 9),
       (15, '#comment15_text#', 'Novikov_Sidor@example.com', 6),
       (16, '#comment16_text#', 'Ivanov_Fedor@example.com', 4),
       (17, '#comment17_text#', 'Kuznetsov_Ivan@example.com', 3),
       (18, '#comment18_text#', 'Novikov_Sidor@example.com', 3),
       (19, '#comment19_text#', 'Sokolov_Nikolaj@example.com', 5),
       (20, '#comment20_text#', 'Kuznetsov_Ivan@example.com', 7);
